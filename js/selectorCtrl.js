/* global data */

var selectorApp = angular.module('selectorApp', []);

selectorApp.$inject = ['$window', 'loginSrv', 'notify'];
selectorApp.controller('selectorCtrl',
//**********************************
// SCOPE
// url:http://web936.login-8.loginserver.ch/_selector2/
//**********************************

        function ($scope) {
            $scope.showPage = false;
            $scope.selectedRow = null;
//            $scope.destinationI$scope.destinationItems = [
//                {
//                    id: 0,
//                    color: 'gelb',
//                    name: 'Basistraining',
//                    url: '../basistraining'
//
//                },
//                {
//                    id: 1,
//                    color: 'grun',
//                    name: 'Aufbautraining',
//                    url: '../aufbautraining'
//                },
//                {
//                    id: 2,
//                    color: '--',
//                    name: 'Individualtraining',
//                    url: '../individualtraining'
//                },
//                {
//                    id: 3,
//                    color: 'lila',
//                    name: 'Praxistag – Neukunden gewinnen',
//                    url: '../neukunden-gewinnen'
//                },
//                {
//                    id: 4,
//                    color: 'blau',
//                    name: 'Praxistag – Angebote nachfassen',
//                    url: '../angebote-nachfassen'
//                },
//                {
//                    id: 5,
//                    color: 'rot',
//                    name: 'Praxistag – Erfolgreich am Telefon',
//                    url: '../erfolgreich-telefonieren'
//                }
//
//            ];tems = [
//                {
//                    id: 0,
//                    color: 'gelb',
//                    name: 'Basistraining',
//                    url: '../basistraining'
//
//                },
//                {
//                    id: 1,
//                    color: 'grun',
//                    name: 'Aufbautraining',
//                    url: '../aufbautraining'
//                },
//                {
//                    id: 2,
//                    color: '--',
//                    name: 'Individualtraining',
//                    url: '../individualtraining'
//                },
//                {
//                    id: 3,
//                    color: 'lila',
//                    name: 'Praxistag – Neukunden gewinnen',
//                    url: '../neukunden-gewinnen'
//                },
//                {
//                    id: 4,
//                    color: 'blau',
//                    name: 'Praxistag – Angebote nachfassen',
//                    url: '../angebote-nachfassen'
//                },
//                {
//                    id: 5,
//                    color: 'rot',
//                    name: 'Praxistag – Erfolgreich am Telefon',
//                    url: '../erfolgreich-telefonieren'
//                }
//
//            ];

            $scope.zoneTable = new ZoneTable(
                    [                       'Aussendienst', 'Innendienst', 'Kundendienst', 'Projektleiter', 'Fachstellen'],
                    [   "Kommunikation",
                        "Kundenorientierung",
                        "Fragetechnik",
                        "Argumentationstechnik",
                        "Bedarfsabklärung",
                        "Einwandbehandlung",
                        "Terminvereinbarung",
                        "Angebote nachfassen",
                        "Preisverhandlungen",
                        "Abschlusstechnik"
                    ]
                    );

//-----------------------------------------
            $scope.zoneTable.defineZone(
                    {
                        id: 0, color: 'gelb', name: 'Basistraining',
                        url: '../basistraining',
                        row1: 0, col1: 0, 
                        row2: 2, col2: 1 
                    }
            );
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 0, color: 'gelb', name: 'Basistraining',
//                        url: '../basistraining',
//                        row1: 1, col1: 1, 
//                        row2: 3, col2: 2 
//                    }
//            );
//-----------------------------------------

    
//-----------------------------------------

            $scope.zoneTable.defineZone(
                    {
                        id: 1, color: 'grun', name: 'Aufbautraining',
                        url: '../aufbautraining',
                        row1: 3, col1: 0, 
                        row2: 5, col2: 1 
                    }
            );

//            $scope.zoneTable.defineZone(
//                    {
//                        id: 1, color: 'grun', name: 'Aufbautraining',
//                        url: '../aufbautraining',
//                        row1: 4, col1: 1, 
//                        row2: 6, col2: 2 
//                    }
//            );
//
//-----------------------------------------

//-----------------------------------------
            $scope.zoneTable.defineZone(
                    {
                        id: 2, color: 'gelbn', name: 'Basistraining',
                        url: '../basistraining',
                        row1: 3, col1: 2, 
                        row2: 5, col2: 4 
                    }
            );
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 2, color: 'gelbn', name: '??',
//                        url: '../??',
//                        row1: 4, col1: 3, 
//                        row2: 6, col2: 5 
//                    }
//            );
//-----------------------------------------

//-----------------------------------------
            $scope.zoneTable.defineZone(
                    {
                        id: 3, color: 'lila', name: 'Praxistag – Neukunden gewinnen',
                        url: '../neukunden-gewinnen',
                        row1: 6, col1: 0, 
                        row2: 6, col2: 4
                    }
            );
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 3, color: 'lila', name: 'Praxistag – Neukunden gewinnen',
//                        url: '../neukunden-gewinnen',
//                        row1: 7, col1: 1, 
//                        row2: 7, col2: 5
//                    }
//            );
//-----------------------------------------

//-----------------------------------------
            $scope.zoneTable.defineZone(
                    {
                        id: 4, color: 'blau', name: 'Praxistag – Angebote nachfassen',
                        url: '../angebote-nachfassen',
                        row1: 7, col1: 0, 
                        row2: 9, col2: 4
                    }
            );
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 4, color: 'blau', name: 'Praxistag – Angebote nachfassen',
//                        url: '../angebote-nachfassen2',
//                        row1: 8, col1: 1, 
//                        row2: 10, col2: 5
//                    }
//            );
//-----------------------------------------


//-----------------------------------------
            $scope.zoneTable.defineZone(
                    {
                        id: 5, color: 'rot', name: 'Praxistag – Erfolgreich am Telefon',
                        url: '../erfolgreich-telefonieren',
                        row1: 0, col1: 2,
                        row2: 2, col2: 4
                    }
            );
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 5, color: 'rot', name: 'Praxistag – Erfolgreich am Telefon',
//                        url: '../erfolgreich-telefonieren',
//                        row1: 1, col1: 3,
//                        row2: 3, col2: 5
//                    }
//            );
//-----------------------------------------

            $scope.destination = "#";
            $scope.selected_cells = [];
            $scope.selected_colors = [];
            $scope.redirect = "";

           /* $scope.z1 = new Zone(
                    {
                        col1: 3, row1: 1,
                        col2: 5, row2: 6,
                        color: 'rot',
                        name: 'Praxistag – Erfolgreich am Telefon',
                        url: '../erfolgreich-telefonieren'
                    }
            );*/



            $scope.getUrl = function (col) {
                console.log(col);

                return {red: col === 0};
            };

        }


// **************
// CONTROLLER END
// **************
);



// ******************
// SCOPE END
// ******************

