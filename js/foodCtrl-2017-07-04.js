var foodApp = angular.module('foodApp',[]);

foodApp.$inject=['$window', 'loginSrv', 'notify'];
foodApp.controller('foodCtrl',

//**********************************
// SCOPE
//**********************************

function($scope){
	$scope.showPage=false;
	$scope.selectedRow = null;
	$scope.destinationItems = [
			{
				id:0,
				color:'gelb',
				name:'Basistraining',
				url:'../basistraining'
				
			},
			{
				id:1,
				color:'grun',
				name:'Aufbautraining',
				url:'../aufbautraining'
			},
			{
				id:2,
				color:'--',
				name:'Individualtraining',
				url:'../individualtraining'
			},
			{
				id:3,
				color:'lila',
				name:'Praxistag – Neukunden gewinnen',
				url:'../neukunden-gewinnen'
			},
			{
				id:4,
				color:'blau',
				name:'Praxistag – Angebote nachfassen',
				url:'../angebote-nachfassen'
			},
			{
				id:5,
				color:'rot',
				name:'Praxistag – Erfolgreich am Telefon',
				url:'../erfolgreich-telefonieren'
			}
	
	];
	
	$scope.destination="#";
	$scope.selected_cells = [];	
	$scope.selected_colors = [];
	$scope.redirect="";

	$scope.z1 = new Zone(
				{
					col1:3,row1:1,
					col2:5,row2:6
					
				}
	);

	$scope.setClickedCell = function(row,col){
		console.clear();
		console.log('r '+row+' c '+col);
		$scope.selectedRow = row;
		$scope.selectedCol = col;
		
		
		var index = $scope.selected_cells.indexOf( (row+':'+col) );
				
		if (index > -1) { // ha van, torli
    		$scope.selected_cells.splice(index, 1);
		} else  { // ha nincs , beleteszi.
			$scope.selected_cells.push( (row+':'+col) ) 
			
		}
		//---------------------
		
		$scope.selected_colors = [];
		$scope.z1.reset();
		
		for(var i=0,j=$scope.selected_cells.length; i<j; i++){
			
		  	cellcoord=$scope.selected_cells[i].split(':');
		  	crow=parseInt(cellcoord[0])+1;
		  	ccol=parseInt(cellcoord[1])+1;
		
			// color couter
			
			var color = $scope.tableType.data.rows[crow][ccol];	
			
			inzone=$scope.z1.inzone(crow,ccol);
			console.log(color+'@ c '+ccol+': r '+crow+' ='+inzone);
			
			
			var index = $scope.selected_colors.indexOf( color );
			if (index > -1) {
	    		//$scope.selected_colors.splice(index, 1);
			} else  { $scope.selected_colors.push( (color) ) };
				
				
		};
		console.log(':::'+$scope.z1.bingo);
		$scope.showPage=$scope.z1.bingo>1;


		if ($scope.showPage) {
			$scope.destination="../basisworkshop-kundenorientierung";
			//window.location.assign("../basisworkshop-kundenorientierung");
		}
		else if ($scope.selected_colors.length>1) {$scope.destination="../individualtraining";} 
		else{
				//$scope.destination=$scope.selected_colors[0];
				$scope.destination = $scope.selected_colors[0];
//				$scope.destination = _.find($scope.destinationItems, {'color':$scope.selected_colors[0] }).url;
				$scope.destination = $scope.destinationItems.find(x=>x.color==$scope.selected_colors[0]).url;
				
			};
		
		
	}	
	
  
  	
  $scope.tableType = {
    data: {
      rows: [
        [""						, 	'Aussendienst',	'Innendienst',	'Kundendienst',	'Projektleiter',	'Fachstellen'	],
        ["Kommunikation"		, 	"gelb", 		"gelb",			"rot",			"rot",				"rot"			],
        ["Kundenorientierung"	,	"gelb", 		"gelb",			"rot",			"rot",				"rot"			],
        ["Fragetechnik"			, 	"gelb", 		"gelb",			"rot",			"rot",				"rot"			],
        ["Argumentationstechnik", 	"grun", 		"grun",			"gelbn",		"gelbn",			"gelbn"			],
        ["Bedarfsabklärung"	,   	"grun", 		"grun",			"gelbn",		"gelbn",			"gelbn"			],
        ["Einwandbehandlung"	, 	"grun", 		"grun",			"gelbn",		"gelbn",			"gelbn"			],
        ["Terminvereinbarung"	, 	"lila", 		"lila",			"lila",			"lila",				"lila"			],
        ["Angebote nachfassen"	, 	"blau", 		"blau",			"blau",			"blau",				"blau",			],
        ["Preisverhandlungen"	, 	"blau", 		"blau",			"blau",			"blau",				"blau",			],
        ["Abschlusstechnik"		, 	"blau", 		"blau",			"blau",			"blau",				"blau",			],
      ]
    }	
	}
	
  $scope.getUrl = function(col){
    console.log(col);
    
    return {red: col == 0};
  };	
	
}


// **************
// CONTROLLER END
// **************
);



// ******************
// SCOPE END
// ******************

