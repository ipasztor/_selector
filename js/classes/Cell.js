/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Cell =
        (function () {
            function Cell(z) {
                //this.table=z.table;
                this.row = z.row;
                this.col = z.col;
                this.color = z.color;
                this.bingo = 0;
                this.klickablee12 = true;
                this.name = z.name;
                this.cid = '[r ' + this.row + ', c ' + this.col + ']' + 'color: ' + this.color + ', bingo: ' + this.bingo;
                this.clickCounter=0;
                this.debugging=false;
                console.log("Cell constructor End:" + this.cid);
            }
            ;

            //Cell.prototype.colorTable = function (table) {this.table[this.row][this.col] = this.color;};
            Cell.prototype.hello = function () {
                var hellotext;
                hellotext=this.color +'('+this.bingo+')'+ ':' + this.row + ':' + this.col+' cnt: '+this.clickCounter;;
                if(this.bingo){
                    console.log(hellotext);
                }
                if(!this.debugging)
                {
                    hellotext='';
                }
                return hellotext;
            };

            Cell.prototype.setColor = function (c) {
                this.color = c;
            };

            Cell.prototype.selected = function () {
                return this.bingo;
            };

            Cell.prototype.setKlickableee = function (val) {
                //this.klickable=val;
            };

            Cell.prototype.unClickMe = function () {
                this.clickCounter+=1;
                this.bingo = 0;
                this.cid = '[r ' + this.row + ', c ' + this.col + ']' + 'color: ' + this.color + ', bingo: ' + this.bingo+' cnt: '+this.clickCounter;
                console.log('cell.unClickMe:' + this.cid);
                return this.bingo;
            };

            Cell.prototype.clickMe = function () {
                this.clickCounter+=1;
                if (this.bingo===1) {
                    this.bingo = 0;
                } else if(this.bingo===0) {
                    //if (this.klickable) {
                    this.bingo = 1;
                    //}
                }
                this.cid = '[r ' + this.row + ', c ' + this.col + ']' + 'color: ' + this.color + ', bingo: ' + this.bingo+' cnt: '+this.clickCounter;
                console.log('cell.ClickMe:' + this.cid);
                return this.bingo;
            };
            return Cell;
        }
        ());
