/**
 * @author hazimunka
 */
// ******************
// COUNTER CLASS
// ******************

// ********

var Zone =
        (function () {
            function Zone(z, celltable) {
                this.table = celltable;
                this.row1 = z.row1;
                this.col1 = z.col1;
                this.row2 = z.row2;
                this.col2 = z.col2;
                this.bingo = 0;
                this.zid = '[r ' + this.row1 + ', c ' + this.col1 + ']' + '[r ' + this.row2 + ', c ' + this.col2 + ']';
                this.selections = {};
                this.color = z.color;
                this.url = z.url;
                this.name = z.name;
                this.id = z.id;
                this.cells = {};
                this.iniCells();

                console.log("Zone constructor End:" + this.zid);
                console.info(celltable);
                console.log('---------- ----------- -------------');
            }
            ;
            Zone.prototype.setTable = function (table) {
                this.table = table;
            };
            
            
            Zone.prototype.iniCells = function () {
                for (var row = this.row1, maxR = this.row2; row <= maxR; row++) {
                    //console.log(this.rH[row]);
                    //this.cells[row] = {};
                    //console.log(this.table[row][0].hello());
                    for (var col = this.col1, maxC = this.col2; col <= maxC; col++) {
                        //var cell=
                        this.table[row][col].setColor(this.color);
                        //Initiating transferred into Zone Table.
                        //this.cells[row][col] = new Cell({row: row, col: col, color: this.color, name:'', table: this.table});
                        //this.cells[row][col] = this.table[row][col];//new Cell({row: row, col: col, color: this.color, name:'', table: this.table});
                        //
                        
                        //this.table[row][col] = 'gelb';//this.color;

                        //console.log(this.rH[row]+': '+this.cH[col]);
                    }

                }
                console.log('Zone.Cells created: %O', this.cells);
                console.info(this.cells);
                console.log('----------------------------------');

            };

            Zone.prototype.colorTable = function () {
                for (var row = this.row1, maxR = this.row2; row <= maxR; row++) {
                    //console.log(this.rH[row]);
                    //this.cells[row]={};
                    for (var col = this.col1, maxC = this.col2; col <= maxC; col++) {
                        //var cell=
                        //this.cells[row][col]=new Cell(   {  row:row, col:col, color:this.color  }  );
                        //this.table[row][col] = this.color;
                        //this.cells[row][col].colorTable();
                        this.cells[row][col].setColor(this.color);
                        //console.log(this.rH[row]+': '+this.cH[col]);
                    }

                }
                console.log('Zone.Cells created: %O', this.cells);
                console.info(this.cells);
                console.log('----------------------------------');

            };

            
            Zone.prototype.setKlickable = function (kval) {
                for (var row = this.row1, maxR = this.row2; row <= maxR; row++) {
                    for (var col = this.col1, maxC = this.col2; col <= maxC; col++) {
                        this.cells[row][col].setKlickable(kval);
                    }
                }
            };

            Zone.prototype.getSelectionCounter = function () {
                //count nr of selected cells !!
                var bCnt,cell;
                bCnt=0;
                
                
                for (var row = this.row1, maxR = this.row2; row <= maxR; row++) {
                    for (var col = this.col1, maxC = this.col2; col <= maxC; col++) {
                        cell=this.table[row][col];
                        bCnt=bCnt+cell.bingo;
                        if (cell.bingo) {
                            console.log('Zone.prototype.getSelectionCounter: Bingo! '+this.zid+' : testing : '+row+':'+col );
                            //console.log('Zone.prototype.getSelectionCounter: bingo :'+row+':'+col);
                        }
                    }

                }
                console.log('BINGO CNT:'+bCnt);
                return bCnt;
            };

            Zone.prototype.has = function (id) {
                return this.id === id;
            };

            Zone.prototype.reset = function () {
                this.bingo = 0;
            };

            Zone.prototype.inzone = function (row, col) {
                console.log ("zone.inzone test:"+this.zid+' : ( r '+row+' c '+col+')');
                //Reduced complexity if deciding here about bingo click, and not transferred this decision into cells.
                //The klick counter may be transferred into cells.
                var inzone = 0;
                if (
                        col >= this.col1 && row >= this.row1
                        && col <= this.col2 && row <= this.row2
                        ) {
                    //console.log('*');
                    inzone = 1;
                    //clicked in zoneTable alredy.
                    //this.cells[row][col].clickMe();
                    console.log('---------------------------------');
                    console.log('Zone.inzone : display info cells:');
                    //console.log('---------------------------------');
                    console.info(this.cells);
                    console.log('---------------------------------');


                    //this.bingo = this.bingo + 1;

                    // is in zone.
                    // process the on /off statut of the cell.
                    // create Cell class ?


                    //console.log('in zone:('+this.col1+'<='+col+'=>'+this.col2+')'  + '(' +this.row1+'<='+row+'>='+this.row2+')'	);

                } else {
                    /*
                     console.log(
                     'OUT OF zone:NOT :('+col+'>='+this.col1+')'  + '(' +row+'>='+this.row1+')'
                     +      ':('+col+'<='+this.col2+')'  + '(' +row+'<='+this.row2+')'
                     
                     );
                     */
                }

                //console.log(this.bingo);
                return this.getSelectionCounter();
            };

            return Zone;
        }());

//var z1 = new Zone(1,3,6,5);
//var zid1 = z1.inzone(2,2);
