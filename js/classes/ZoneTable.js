/**
 * @author hazimunka
 */
// ******************
// COUNTER CLASS
// ******************

// ********

var ZoneTable =
        (function () {
            function ZoneTable(cHeads, rHeads) {
                var tRow;
                this.cH = cHeads;
                this.rH = rHeads;
                this.table = [];//colored display table info // rewrite as Cells.
                this.selection = [];//selected zones
                this.selectionCount = 0;//selected zones
                this.zones = [];//zones defined
                this.url='';
                this.urlMultipleSelections='../individualtraining';
                this.noSelectionAlertMessage='Please select at least one cell.';
                this.debugging=false;
                
                //create table :
                for (var row = 0, rMax = rHeads.length; row < rMax; row++) {
                    //annyi sorunk lesz ahany columnHeads van.
                    //console.log(this.rH[row]);
                    //this.table.push(new Array(this.cH.length));
                    tRow = [];
                    for (var col = 0, cMax = cHeads.length; col < cMax; col++) {
                        tRow.push(new Cell({row: row, col: col, color: 'white', name: (rHeads[row]) + ':' + cHeads[col]}));
                    }
                    this.table.push(tRow);

                }
                /*
                 
                 for (var row = 1, maxR = this.rH.length; row < maxR; row++) {
                 //console.log(this.rH[row]);
                 
                 for (var col = 0, maxC = this.cH.length; col < maxC; col++) {
                 
                 //creating the whiteboard:
                 this.table[row][col] = 'white';// ??::new Cell({row: row, col: col, color: this.color, table: this.table});
                 
                 //this.table[row][col]=row+':'+col+' '+this.table[row][col];
                 
                 // writing the heads of rows
                 if (col === 0) {
                 this.table[row][0] = this.rH[row];
                 }
                 
                 //console.log(this.rH[row]+': '+this.cH[col]);
                 }
                 
                 }
                 */
                this.bingo = 0;
                this.zid = 'ZoneTable';

                this.url = '';
                this.name = 'no_name';

                console.info(this.table);
                console.log("ZoneTable constructor End:" + this.zid);
            }
            ;

            ZoneTable.prototype.getCell = function (row, col) {
                //console.log('GetCell ['+row+':'+col+'] request :');
                cell = this.table[row][col];
                cell.debugging=this.debugging;
                //console.log('GetCell ['+row+':'+col+'] respose :'+cell.hello());
                //console.log(cell.hello());

                return cell;
            }


            ZoneTable.prototype.defineZone = function (z) {
                console.log('DZ!!' + z.color);
                var zone = new Zone(z, this.table);
                this.zones.push(zone);
                return 1;
            }

            ;
            /**************************************
             * resetting all elements to: bingo=0
             * */
            ZoneTable.prototype.reset = function () {
                ;
            };

//-----------------------------------
//
//  in zone
//
//-----------------------------------

            // testing click coords against all zones
            ZoneTable.prototype.inzone = function (row, col) {

                var z, bCnt;

                this.selection = [];
                this.selectionCount=0;
                
                for (var i = 0, max = this.zones.length; i < max; i++) {
                    z = this.zones[i];

                    //console.log(this.zones.length + ':' + i);
                    //console.log('inzone test: klick@:( ' + row + ',' + col + ' ): ZoneTested : ' + z.color + '[ ' + z.row1 + ',' + z.col1 + ' ]' + '[ ' + z.row2 + ',' + z.col2 + ' ]');

                    bCnt = z.getSelectionCounter();
                    
                    if (bCnt > 0) {
                        this.selection.push({zone: z, color:z.color, counter: bCnt, url:z.url});
                        this.selectionCount+=bCnt;
                    } // selection status : {selection:0..x, being the nr of selected cells in that zone.}
                }

                //console.log(this.bingo);
                console.log('ZoneTable.inzone :__________');
                console.info(this.selection);
                console.log('ZoneTable.inzone :__________');
                return this.selection;
            };
//-----------------------------------
//
//  in zone end.
//
//-----------------------------------
//-----------------------------------
//
//  Klcikable
//
//-----------------------------------

            // testing click coords against all zones
            ZoneTable.prototype.setKlickablee = function (kval) {
//                console.clear();
//                var z;
//
//                
//                for (var i = 0, max = this.zones.length; i < max; i++) {
//                    z = this.zones[i];
//                    z.setKlickable(kval);
//                }
            };
//-----------------------------------
//
//  Klcikable end.
//
//-----------------------------------
//-----------------------------------
//
//  ClickAt
//
//-----------------------------------

            // click cell and informing zones, testing click coords against all zones
            ZoneTable.prototype.clickAt = function (row,col) {
                
                console.clear();
                if (this.selectionCount<3){
                    this.table[row][col].clickMe();
                }else {
                    this.table[row][col].unClickMe();
                }
                
                this.inzone(row,col); 
                
                // select the requested url.
                this.url=this.decision();
                
            };
//-----------------------------------
//
//  ClickAt.
//
//-----------------------------------

//-----------------------------------
//
//  Decision
//
//-----------------------------------

            // click cell and informing zones, testing click coords against all zones
            ZoneTable.prototype.decision = function () {
                
                var url;
                var maxE;
                url='';
                maxE=this.maxElem();
                
                if (this.isMultipleSelection()){
                    url=this.urlMultipleSelections;
                    // is maxElem, 2 click vs 1 ?, or all are 1 click.
                    if (maxE.color==='maxElem'){
                        // there is not max elem, all are 1 click. url remains indivdualtraining.
                        url=this.urlMultipleSelections;
                    }else{
                         // there is a max elem, url will be the maximum selected.
                        url=maxE.url;
                    }
                    
                }else if (this.selection.length===1){ // is selected a single cell.
                    url=this.selection[0].url;
                //Wenn mehr als 2 aus dem roten Feld gewählt werden, geht eine Subpage auf, welche wie z.B. das Basisprodukt aufgebaut ist.  Diese Seite beinhaltet ein Basisworkshop  Kundenorientierung.
                //If more than 2 are selected from the red field, a subpage will appear, which, as e.g. The basic product. This page contains a basic workshop customer orientation.                
                    if((this.selection[0].color==='rot') && (this.selection[0].counter===3) ){
                        
                        url='../basistraining';
                    }
                
                }else{
                    // no selection. Alert : please select at least one option.
                    //alert(this.noSelectionAlertMessage);
                    url="";
                }
                
                
                return url;
            };

            ZoneTable.prototype.isMultipleSelection = function () {
                return this.selection.length>1;
            };
            
            ZoneTable.prototype.maxElem = function () {
                
                var maxE;
                
                maxE={zone:null,url:'',counter:1,color:'maxElem'};
                for (var i = 0, max = this.selection.length; i < max; i++) {
                    if(this.selection[i].counter>maxE.counter){maxE=this.selection[i];};
                }



                return maxE;
            };            
//-----------------------------------
//
//  Decision end.
//
//-----------------------------------

//-----------------------------------
//
//  selectUrl
//
//-----------------------------------


//-----------------------------------
//
//  selectUrl end.
//
//-----------------------------------

            ZoneTable.prototype.getSelectedZones = function () {
                console.log('Z.getSelectedZones');
                return this.selection;
            };

            ZoneTable.prototype.getSelectedCells = function () {
                console.log('getSelectedCells');
                return this.selection;
            };

            
            

            ZoneTable.prototype.getSelectedColors = function () {
                return this.selection;
            };

            ZoneTable.prototype.getZoneByColor = function () {
                return this.selection;
            };

            ZoneTable.prototype.getTable = function () {
                return this.table;
            };

            return ZoneTable;
        }());

//var z1 = new Zone(1,3,6,5);
//var zid1 = z1.inzone(2,2);
