/* global data */

var foodApp = angular.module('foodApp', []);

foodApp.$inject = ['$window', 'loginSrv', 'notify'];
foodApp.controller('foodCtrl',
//**********************************
// SCOPE
//**********************************

        function ($scope) {
            $scope.showPage = false;
            $scope.selectedRow = null;
            $scope.destinationItems = [
                {
                    id: 0,
                    color: 'gelb',
                    name: 'Basistraining',
                    url: '../basistraining'

                },
                {
                    id: 1,
                    color: 'grun',
                    name: 'Aufbautraining',
                    url: '../aufbautraining'
                },
                {
                    id: 2,
                    color: '--',
                    name: 'Individualtraining',
                    url: '../individualtraining'
                },
                {
                    id: 3,
                    color: 'lila',
                    name: 'Praxistag – Neukunden gewinnen',
                    url: '../neukunden-gewinnen'
                },
                {
                    id: 4,
                    color: 'blau',
                    name: 'Praxistag – Angebote nachfassen',
                    url: '../angebote-nachfassen'
                },
                {
                    id: 5,
                    color: 'rot',
                    name: 'Praxistag – Erfolgreich am Telefon',
                    url: '../erfolgreich-telefonieren'
                }

            ];

            $scope.zoneTable = new ZoneTable(
                    ["", 'Aussendienst', 'Innendienst', 'Kundendienst', 'Projektleiter', 'Fachstellen'],
                    ["","Kommunikation",
                        "Kundenorientierung",
                        "Fragetechnik",
                        "Argumentationstechnik",
                        "Bedarfsabklärung",
                        "Einwandbehandlung",
                        "Terminvereinbarung",
                        "Angebote nachfassen",
                        "Preisverhandlungen",
                        "Abschlusstechnik"
                    ]
                    );

//-----------------------------------------
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 0, color: 'gelb', name: 'Basistraining',
//                        url: '../basistraining',
//                        row1: 0, col1: 0, 
//                        row2: 2, col2: 1 
//                    }
//            );
            $scope.zoneTable.defineZone(
                    {
                        id: 0, color: 'gelb', name: 'Basistraining',
                        url: '../basistraining',
                        row1: 1, col1: 1, 
                        row2: 3, col2: 2 
                    }
            );
//-----------------------------------------

    
//-----------------------------------------
//
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 1, color: 'grun', name: 'Aufbautraining',
//                        url: '../aufbautraining',
//                        row1: 3, col1: 0, 
//                        row2: 5, col2: 1 
//                    }
//            );

            $scope.zoneTable.defineZone(
                    {
                        id: 1, color: 'grun', name: 'Aufbautraining',
                        url: '../aufbautraining',
                        row1: 4, col1: 1, 
                        row2: 6, col2: 2 
                    }
            );
//
//-----------------------------------------

//-----------------------------------------
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 2, color: 'gelbn', name: '??',
//                        url: '../??',
//                        row1: 3, col1: 2, 
//                        row2: 5, col2: 4 
//                    }
//            );
            $scope.zoneTable.defineZone(
                    {
                        id: 2, color: 'gelbn', name: '??',
                        url: '../??',
                        row1: 4, col1: 3, 
                        row2: 6, col2: 5 
                    }
            );
//-----------------------------------------

//-----------------------------------------
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 3, color: 'lila', name: 'Praxistag – Neukunden gewinnen',
//                        url: '../neukunden-gewinnen',
//                        row1: 6, col1: 0, 
//                        row2: 6, col2: 4
//                    }
//            );
            $scope.zoneTable.defineZone(
                    {
                        id: 3, color: 'lila', name: 'Praxistag – Neukunden gewinnen',
                        url: '../neukunden-gewinnen',
                        row1: 7, col1: 1, 
                        row2: 7, col2: 5
                    }
            );
//-----------------------------------------

//-----------------------------------------
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 4, color: 'blau', name: 'Praxistag – Angebote nachfassen',
//                        url: '../angebote-nachfassen2',
//                        row1: 7, col1: 0, 
//                        row2: 9, col2: 4
//                    }
//            );
            $scope.zoneTable.defineZone(
                    {
                        id: 4, color: 'blau', name: 'Praxistag – Angebote nachfassen',
                        url: '../angebote-nachfassen2',
                        row1: 8, col1: 1, 
                        row2: 10, col2: 5
                    }
            );
//-----------------------------------------


//-----------------------------------------
//            $scope.zoneTable.defineZone(
//                    {
//                        id: 5, color: 'rot', name: 'Praxistag – Erfolgreich am Telefon',
//                        url: '../erfolgreich-telefonieren',
//                        row1: 0, col1: 2,
//                        row2: 2, col2: 4
//                    }
//            );
            $scope.zoneTable.defineZone(
                    {
                        id: 5, color: 'rot', name: 'Praxistag – Erfolgreich am Telefon',
                        url: '../erfolgreich-telefonieren',
                        row1: 1, col1: 3,
                        row2: 3, col2: 5
                    }
            );
//-----------------------------------------

            $scope.destination = "#";
            $scope.selected_cells = [];
            $scope.selected_colors = [];
            $scope.redirect = "";

            $scope.z1 = new Zone(
                    {
                        col1: 3, row1: 1,
                        col2: 5, row2: 6,
                        color: 'rot',
                        name: 'Praxistag – Erfolgreich am Telefon',
                        url: '../erfolgreich-telefonieren'
                    }
            );

            $scope.setClickedCell = function (row, col) {
                console.clear();
                console.log('r ' + row + ' c ' + col);
                $scope.zoneTable.inzone(row,col);
                    // TESTING IN ZONES
                    // CONSTRUCT THE SELECTED CELLS LIST
                        //MEMORIZE THE SELECTED CELL
                        //INCLUDE IN THE LIST // ERASE FROM THE LIST // DISPLAY THE LIST
                    
                
                //--------------------------------------------------------------
                
                $scope.selectedRow = row;
                $scope.selectedCol = col;

                //searching in the stack
                var index = $scope.selected_cells.indexOf((row + ':' + col));

                //if exists, erasing ; if isn't, pushing.
                if (index > -1) { // ha van, torli
                    $scope.selected_cells.splice(index, 1);
                } else { // ha nincs , beleteszi.
                    if ($scope.selected_cells.length < 3) {
                        $scope.selected_cells.push((row + ':' + col));
                    }
                }
                //---------------------

                $scope.selected_colors = [];
                $scope.z1.reset();

                /*if ($scope.selected_cells.length>3) {
                 console.log($scope.selected_cells.length);
                 console.log('!!!');
                 return {red: col == 0};
                 }*/

                for (var i = 0, j = $scope.selected_cells.length; i < j; i++) {

                    cellcoord = $scope.selected_cells[i].split(':');
                    crow = parseInt(cellcoord[0]) + 1;
                    ccol = parseInt(cellcoord[1]) + 1;

                    // color couter

                    var color = $scope.tableType.data.rows[crow][ccol];


                    // testing and counting click@ zone 1 : [3,1]:[5,6]
                    // if we will had more than one click at this zone, the destination will be : basisworkshop-kundenorientierung
                    // if we will had less than one click at this zone, the destination will be : 
                    //                                                 if nr of selected colors are>1 then individualtraining, 
                    //                                                 if nr of selected colors =1 then the color specified destination

                    inzone = $scope.z1.inzone(crow, ccol);
                    console.log(color + '@ c ' + ccol + ': r ' + crow + ' =' + inzone);


                    var index = $scope.selected_colors.indexOf(color);
                    if (index > -1) {
                        //$scope.selected_colors.splice(index, 1);
                        //we have to count here the number ? when to reset ?
                    } else {
                        $scope.selected_colors.push((color));
                    }
                    ;


                }
                ;

                console.log(':::' + $scope.z1.bingo);
                $scope.showPage = $scope.z1.bingo > 1;


                if ($scope.showPage) {
                    $scope.destination = "../basisworkshop-kundenorientierung";
                    //window.location.assign("../basisworkshop-kundenorientierung");
                } else if ($scope.selected_colors.length > 1) {
                    $scope.destination = "../individualtraining";
                } else {
                    //$scope.destination=$scope.selected_colors[0];
                    $scope.destination = $scope.selected_colors[0];
//				$scope.destination = _.find($scope.destinationItems, {'color':$scope.selected_colors[0] }).url;
                    $scope.destination = $scope.destinationItems.find(x => x.color === $scope.selected_colors[0]).url;

                }
                ;


            };



            $scope.tableType = {
                data: {
                    rows: [
                        ["", 'Aussendienst', 'Innendienst', 'Kundendienst', 'Projektleiter', 'Fachstellen'],
                        ["Kommunikation",           "gelb", "gelb", "rot", "rot", "rot"],
                        ["Kundenorientierung",      "gelb", "gelb", "rot", "rot", "rot"],
                        ["Fragetechnik",            "gelb", "gelb", "rot", "rot", "rot"],
                        ["Argumentationstechnik",   "grun", "grun", "gelbn", "gelbn", "gelbn"],
                        ["Bedarfsabklärung",        "grun", "grun", "gelbn", "gelbn", "gelbn"],
                        ["Einwandbehandlung",       "grun", "grun", "gelbn", "gelbn", "gelbn"],
                        ["Terminvereinbarung",      "lila", "lila", "lila", "lila", "lila"],
                        ["Angebote nachfassen",     "blau", "blau", "blau", "blau", "blau"],
                        ["Preisverhandlungen",      "blau", "blau", "blau", "blau", "blau"],
                        ["Abschlusstechnik",        "blau", "blau", "blau", "blau", "blau"]
                    ]
                }
            };


            $scope.tableType.data.rows = $scope.zoneTable.getTable();

            $scope.getUrl = function (col) {
                console.log(col);

                return {red: col === 0};
            };

        }


// **************
// CONTROLLER END
// **************
);



// ******************
// SCOPE END
// ******************

